import type { GatsbyNode } from "gatsby";
import path from "path";

export const onCreateWebpackConfig: GatsbyNode["onCreateWebpackConfig"] = ({
  actions,
}) => {
  actions.setWebpackConfig({
    resolve: {
      alias: {
        "~": path.resolve(__dirname, "src"),
      },
    },
  });
};

export const onCreateNode: GatsbyNode["onCreateNode"] = ({
  node,
  getNode,
  actions,
}) => {
  const { createNodeField } = actions;
  if (node.internal.type === `AlbumsJson`) {
    createNodeField({
      node,
      name: `slug`,
      value: `${node.reference_number}`,
    });
  }
};

export const createPages: GatsbyNode["createPages"] = async ({
  graphql,
  actions,
}) => {
  const { createPage } = actions;
  const result: any = await graphql(`
    query {
      allAlbumsJson {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
    }
  `);
  result.data.allAlbumsJson.edges.forEach(
    ({
      node,
    }: {
      node: {
        fields: {
          slug: string;
        };
      };
    }) => {
      createPage({
        path: node.fields.slug,
        component: path.resolve(`./src/templates/album/album.jsx`),
        context: {
          // Data passed to context is available
          // in page queries as GraphQL variables.
          slug: node.fields.slug,
        },
      });
    },
  );
};
