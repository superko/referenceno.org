import React from "react";
import { graphql } from "gatsby";
import Container from "~/components/Container/Container";
import { dataHeadingsMap } from "~/helpers";
import { getImage } from "gatsby-plugin-image";
import * as styles from "./idx.module.scss";

const columns = [
  "reference_number",
  "source",
  "title",
  "form",
  "edition",
  "issued",
  "image",
];

export default function Idx({ data }) {
  const albums = data.allAlbumsJson.edges;
  const allArt = data.allImageSharp.nodes;
  const tableMinSize = 5;

  return (
    <Container>
      <table className={styles.albumIndex}>
        <thead>
          <tr>
            {columns.map((column) => (
              <td key={column} className={styles[column]}>
                {dataHeadingsMap[column]}
              </td>
            ))}
          </tr>
        </thead>
        <tbody>
          {albums.map(({ node }) => {
            const artNode = allArt.find((artNode) => {
              return artNode.gatsbyImageData.images.fallback.src.includes(
                `/${node.image_filenames[0]}.`,
              );
            });
            const image = getImage(artNode);
            return (
              <tr key={node.reference_number}>
                {columns.map((column) => (
                  <td key={column} className={styles[column]}>
                    {column === "image" ? (
                      <a href={`/${node.fields.slug}`}>
                        <img
                          src={image.images.fallback.src}
                          alt={`Art for ${node.title}`}
                        />
                      </a>
                    ) : column === "reference_number" || column === "title" ? (
                      <a href={`/${node.fields.slug}`}>{node[column]}</a>
                    ) : column === "form" ? (
                      <a
                        href={node.bandcamp.url}
                        target="_blank"
                        rel="noreferrer"
                      >
                        {node[column]}
                      </a>
                    ) : (
                      node[column]
                    )}
                  </td>
                ))}
              </tr>
            );
          })}
          {albums.length < tableMinSize
            ? new Array(tableMinSize - albums.length).fill(1).map((a, i) => (
                <tr key={i}>
                  {columns.map((column) => (
                    <td key={column} className={styles[column]} />
                  ))}
                </tr>
              ))
            : null}
        </tbody>
      </table>
    </Container>
  );
}

export const query = graphql`
  query {
    allAlbumsJson(sort: { issued: ASC }) {
      edges {
        node {
          reference_number
          source
          title
          form
          edition
          issued
          image_filenames
          fields {
            slug
          }
          bandcamp {
            url
          }
        }
      }
    }
    allImageSharp {
      nodes {
        gatsbyImageData(width: 300, formats: [JPG])
      }
    }
  }
`;

export { Head } from "../components/Head/Head";
