import React from "react";
import Container from "~/components/Container/Container";

export default function Contact() {
  return (
    <Container>
      <p>"Music from behind the speakers."</p>
      <p>
        <a href="mailto:info@referenceno.org">info@referenceno.org</a>
      </p>
      ___
      <p>
        Designed by{" "}
        <a href="https://bureau60a.com" target="_blank" rel="noreferrer">
          bureau60a
        </a>
        . Developed by{" "}
        <a href="https://www.superko.org" target="_blank" rel="noreferrer">
          SUPERKO
        </a>
        .
      </p>
    </Container>
  );
}

export { Head } from "../components/Head/Head";
