import React from "react";
import { graphql } from "gatsby";
import { randomInt, randomIntString } from "~/helpers";
import Container from "~/components/Container/Container";
import Logo from "~/components/Logo/Logo";
import * as styles from "./index.module.scss";

const links = [
  {
    text: "Repository",
    url: "https://refno.bandcamp.com",
    target: "_blank",
    rel: "noreferrer",
  },
  {
    text: "Index",
    url: "/idx",
  },
  {
    text: "Contact",
    url: "/contact",
  },
];

const grid = new Array(16 * 40).fill(1).map(() => randomIntString(999999999));

export default function Home({ data }) {
  const albums = data.allAlbumsJson.edges;

  albums.forEach(({ node }) => {
    links.push({
      text: node.reference_number,
      url: node.fields.slug,
      className: "albumLink",
    });
  });

  // splice links into the random number grid
  grid.splice(0, 1, <Logo />);
  const usedIndices = [0];
  links.forEach((link) => {
    let i = 0;
    while (usedIndices.includes(i)) {
      i = randomInt(grid.length);
    }
    usedIndices.push(i);
    grid.splice(
      i,
      1,
      <a
        href={link.url}
        target={link.target}
        rel={link.rel}
        className={styles[link.className]}
      >
        {link.text}
      </a>,
    );
  });

  return (
    <Container noHeader>
      <div className={styles.root}>
        {grid.map((n, i) => (
          <Text key={i}>{n}</Text>
        ))}
      </div>
    </Container>
  );
}

function Text({ children }) {
  return <span className={styles.text}>{children}</span>;
}

export const query = graphql`
  query {
    allAlbumsJson {
      edges {
        node {
          reference_number
          fields {
            slug
          }
        }
      }
    }
  }
`;

export { Head } from "../components/Head/Head";
