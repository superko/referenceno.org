export function unsnake(s: string) {
  return s.split("_").join(" ").toUpperCase();
}

export function randomInt(max: number) {
  return Math.floor(Math.random() * max);
}

export function randomIntString(max: number) {
  const num = randomInt(max).toString();
  const maxLength = max.toString().length;
  const zeroes =
    num.length < maxLength
      ? new Array(maxLength - num.length).fill(0).join("")
      : "";
  return `${zeroes}${num}`;
}

export const dataHeadingsMap = {
  reference_number: "Reference Number",
  source: "Source",
  title: "Title",
  form: "Form",
  edition: "Edition",
  issued: "Issued",
  image: "View",
  contents: "Contents",
};
