import React from "react";
import Header from "~/components/Header/Header";
import * as styles from "./Container.module.scss";

export default function Container({ children, noHeader }) {
  return (
    <div className={styles.root}>
      {noHeader ? null : <Header />}
      {children}
    </div>
  );
}
