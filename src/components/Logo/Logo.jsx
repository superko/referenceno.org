import React from "react";
import * as styles from "./Logo.module.scss";

function H1() {
  return <h1 className={styles.h1}>Ref. No.</h1>;
}

export default function Logo({ linkToHome }) {
  return linkToHome ? (
    <a href="/" className={styles.link}>
      <H1 />
    </a>
  ) : (
    <H1 />
  );
}
