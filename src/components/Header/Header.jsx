import React from "react";
import Logo from "~/components/Logo/Logo";
import Menu from "~/components/Menu/Menu";
import * as styles from "./Header.module.scss";

export default function Header() {
  return (
    <div className={styles.header}>
      <Logo linkToHome />
      <Menu />
    </div>
  );
}
