import * as React from "react";

export const Head = () => {
  return (
    <>
      <meta charSet="utf-8" />
      <title>Ref. No.</title>
      <meta property="og:title" content="Ref. No." />
      <meta name="description" content="Music from outside the speakers." />
      <meta
        property="og:description"
        content="Music from outside the speakers."
      />
      <meta name="format-detection" content="telephone=no" />
    </>
  );
};
