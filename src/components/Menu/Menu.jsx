import React from "react";
import * as styles from "./Menu.module.scss";

export default function Menu() {
  return (
    <div className={styles.root}>
      <div className={styles.menuButton} tabIndex={0}>
        <div>&#8225;</div>
      </div>
      <menu className={styles.content}>
        <a href="/idx">Index</a>
        <a href="https://refno.bandcamp.com" target="_blank" rel="noreferrer">
          Repository
        </a>
        <a href="/contact">Contact</a>
      </menu>
    </div>
  );
}
