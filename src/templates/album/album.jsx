import React from "react";
import { graphql } from "gatsby";
import { dataHeadingsMap } from "~/helpers";
import Container from "~/components/Container/Container";
import { getImage } from "gatsby-plugin-image";
import * as styles from "./album.module.scss";

export default function Album({ data }) {
  const album = data.albumsJson;
  const allArt = data.allImageSharp.nodes;
  const artNodes = album.image_filenames.map((filename) => {
    return allArt.find((artNode) => {
      return artNode.gatsbyImageData.images.fallback.src.includes(
        `/${filename}.`,
      );
    });
  });
  const images = artNodes.map((node) => getImage(node));

  return (
    <Container>
      <div className={styles.root}>
        <div>
          {images.map((img, i) => (
            <img
              key={i}
              src={img.images.fallback.src}
              alt={`Art for ${album.title}`}
              className={styles.imageContainer}
            />
          ))}
        </div>
        <div>
          {[
            "reference_number",
            "source",
            "title",
            "form",
            "edition",
            "issued",
            "contents",
          ].map((entry) => {
            if (entry === "form") {
              return (
                <InfoLine
                  key={entry}
                  title={dataHeadingsMap[entry]}
                  content={
                    <a
                      href={album.bandcamp.url}
                      target="_blank"
                      rel="noreferrer"
                    >
                      {album[entry]}
                    </a>
                  }
                />
              );
            }
            return (
              <InfoLine
                key={entry}
                title={dataHeadingsMap[entry]}
                content={album[entry]}
              />
            );
          })}

          <div className={styles.description}>
            {album.description.map((p, i) => (
              <p key={i}>{p}</p>
            ))}
          </div>

          <div className={styles.playerContainer}>
            <iframe
              style={{
                border: 0,
                width: "100%",
                height: `${album.bandcamp.player_height || 200}px`,
              }}
              src={`https://bandcamp.com/EmbeddedPlayer/album=${album.bandcamp.album_id}/size=large/bgcol=ffffff/linkcol=333333/artwork=none/transparent=true/`}
              seamless
              title="Bandcamp player"
            >
              <a
                href={album.bandcamp.url}
              >{`${album.title} by ${album.source}`}</a>
            </iframe>
          </div>
        </div>
      </div>
    </Container>
  );
}

function InfoLine({ title, content }) {
  return (
    <div className={styles.infoLine}>
      {title ? <div className={styles.infoTitle}>{title}</div> : null}
      <div className={styles.infoContent}>
        {Array.isArray(content) ? content.map(contentToParagraph) : content}
      </div>
    </div>
  );
}

function contentToParagraph(content, index) {
  return (
    <div className={styles.trackList}>
      <span className={styles.trackNumber}>{index + 1}</span>
      <span className={styles.trackTitle}>{content[0]}</span>
      <span className={styles.trackLength}>{content[1]}</span>
    </div>
  );
}

export const query = graphql`
  query ($slug: String!) {
    albumsJson(fields: { slug: { eq: $slug } }) {
      reference_number
      image_filenames
      source
      title
      form
      edition
      issued
      contents
      description
      bandcamp {
        album_id
        url
        player_height
      }
    }
    allImageSharp {
      nodes {
        gatsbyImageData(layout: FULL_WIDTH, formats: [JPG])
      }
    }
  }
`;

export { Head } from "../../components/Head/Head";
