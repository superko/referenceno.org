# Ref. No.

https://referenceno.org

The Ref. No. website is built with [Gatsby](https://gatsbyjs.com), a static site generator. Gatsby's docs are very thorough if you'd like to understand how it works. For a quick walkthrough on working with this specific site, follow along below.

## Installation

You need to have [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) installed. I highly recommend using [n](https://github.com/tj/n#installation).

Clone this repo to your machine. Then, with the command line at the project's root directory, run `npm install`.

## Development

You can run a local dev server with the command `npm run develop` or `npm start`. This will launch the development version of the site at the url `localhost:8000` which will hot-reload with your code changes.

## Building the site

Ensure you have the latest code with `git fetch && git checkout main && git pull`, followed by `npm install` to install the dependencies.

With the command line at the project's root directory, run `npm run build`. It will give you a success message if successful.

The static site files are in the folder `public/`. You can view each HTML file in your browser locally, though the links probably won't work because relative paths are not set up.

If everything looks good, then you can copy the contents of `public/` to your webhost.

Sometimes it may be necessary to clean up the build files. This can be done by manually deleting the `public/` folder, or running `npm run clean`.

## Content management

### Adding or editing album data

All of the site data is located in the folder `src/data/albums/`. Each file represents one album. To add a new album, create a new file in that folder with any filename (the reference number is recommended but not necessary), with the extension `.json`.

Then, fill in all of the fields with the album's info. Every field is required.

e.g.

```001.json
{
  "reference_number": "001",
  "image_filenames": ["file1", "file2"],
  "source": "Q2",
  "title": "Forward Guidance",
  "form": "T-shirt / Digital",
  "edition": "50",
  "issued": "2020-06-16",
  "contents": [
    ["Cubiclecore", "6:14"],
    ["Phluber (Béton Bruin Re-Architecture)", "5:53"],
    ["Sansculottes", "6:01"]
  ],
  "description": [
    "Performed live by if then do at the M70 Lab, Artengine, Ottawa, 31.05.2007. Edited and produced 2008/2009. Mastered by Luc Grégoire.",
    "Track 2 constructed with processed material from \"Phluber\" by Élément Kuuda"
  ],
  "bandcamp": {
    "url": "https://hdeheutz.bandcamp.com/album/the-natural-world",
    "album_id": "2795654647",
    "player_height": "200
  }
}

```

### Important content notes

- For the album artwork, place the image(s) in the folder `src/data/img`.
  - Fill in `image_filenames` array with the filenames **without the extension**, as Gatsby automatically converts the file into multiple other optimized formats.
  - The first image in the array is the one that will be used on the Index page.
- The `issued` date needs to be in the format `YYYY-MM-DD`, with the hyphens.
- You'll need to track down the `bandcamp.album_id` manually. Go to the album's Bandcamp page and follow the "Share / Embed" flow; in the embed URL that's generated, there will be a property like `album=000000`. This is the `album_id` that you need to enter here.
- The `contents` and `description` fields are arrays (identified by the square brackets `[]`). They are formatted as follows:
  - For `contents`, each array element is itself an array, where the first item is the song title and the second item is the song length.
  - For `description`, each array element is a string representing one paragraph of the description text.
    - If you need to use quotation marks, make sure you escape the quotation mark character with a backslash (i.e. a `\"` here will appear on the site as `"`).
